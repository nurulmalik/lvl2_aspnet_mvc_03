﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_03.Models;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        Db_Customer_2Entities dbModel = new Db_Customer_2Entities();

        // GET: Customer
        public ActionResult Index()
        {
            return View(dbModel.Tbl_Customer.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Tbl_Customer customer)
        {
            using (var transaction = dbModel.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        dbModel.Tbl_Customer.Add(customer);
                        dbModel.SaveChanges();
                        transaction.Commit();
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    Db_Customer_2Entities dbmodel1 = new Db_Customer_2Entities();
                    Tbl_log_history errors = new Tbl_log_history
                    {
                        ErrorMessage = e.Message,
                        ErrorDate = DateTime.Now,
                        ErrorPosition = "Create_Username"
                    };
                    dbmodel1.Tbl_log_history.Add(errors);
                    dbmodel1.SaveChanges();
                    transaction.Rollback();
                    ViewBag.MsgError = e.Message;
                    return View();
                }
            }
            
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(dbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault());
        }

        public ActionResult Edit(int id)
        {
            return View(dbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(Tbl_Customer customer)
        {
            try
            {
                dbModel.Entry(customer).State = EntityState.Modified;
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id) {
            return View(dbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault());
         }

        [HttpPost]
        public ActionResult Delete(int id, Tbl_Customer customer)
        {
            try
            {
                customer = dbModel.Tbl_Customer.Where(x => x.Id == id).FirstOrDefault();
                dbModel.Tbl_Customer.Remove(customer);
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}